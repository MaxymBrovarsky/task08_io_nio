package com.brom.epam.nioclient.view;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private InetSocketAddress inetSocketAddress;
  private SocketChannel socketChannel;
  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(View.class.getName());
  public View() throws IOException {
    inetSocketAddress = new InetSocketAddress("localhost", 8080);
    socketChannel = SocketChannel.open(inetSocketAddress);
  }

  public void start() throws IOException {
    Selector selector = Selector.open();
    socketChannel.configureBlocking(false);
    socketChannel.register(selector, SelectionKey.OP_READ);
    socketChannel.register(selector, SelectionKey.OP_WRITE);
    logger.info("Hello!!! Please enter your Name");
    String name = input.nextLine();
    ByteBuffer buffer = ByteBuffer.wrap(formSetNameCommand(name).getBytes());
    socketChannel.write(buffer);
    buffer.clear();
    logger.info("Enter message in format <receiver nickname>: <message>");
    Timer timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(256);
        try {
          socketChannel.read(byteBuffer);
        } catch (IOException e) {
          e.printStackTrace();
        }
        String fromServer = new String(byteBuffer.array()).trim();
        if (!fromServer.isEmpty()) {
          System.out.println(fromServer);
          byteBuffer.clear();
        }
      }
    }, 1000, 1000);
    while (true) {
      String message = input.nextLine();
      String[] messParts = message.split(":");
      buffer = ByteBuffer.wrap(formSendMessageCommand(messParts[0], messParts[1]).getBytes());
      socketChannel.write(buffer);
    }
  }

  private String formSetNameCommand(String name) throws IOException {
    return "SETNAME;" + name;
  }
  private String formSendMessageCommand(String name, String message) throws IOException {
    return "SENDMESSAGE;" + name + ";" + message;
  }
}
