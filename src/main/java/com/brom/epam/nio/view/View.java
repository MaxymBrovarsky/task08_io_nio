package com.brom.epam.nio.view;

import com.brom.epam.nio.controller.Controller;
import com.brom.epam.nio.controller.ControllerProgressListener;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View implements ControllerProgressListener {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    initMenu();
    initMenuCommands();
    controller = new Controller();
    controller.addProgressListener(this);
  }

  private void initMenu() {
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Serialization task");
    menu.put("2", "2. Comparing usual and buffered reader task");
    menu.put("3", "3. Example of pushback stream");
    menu.put("4", "4. Show all comments");
    menu.put("5", "5. Show directory content");
    menu.put("5", "5. Show directory content with NIO");
  }

  private void initMenuCommands() {
    menuCommands = new LinkedHashMap<>();
    menuCommands.put("1", this::showSerializationExample);
    menuCommands.put("2", this::showComparingUsualAndBufferedReaders);
    menuCommands.put("3", this::showPushbackStream);
    menuCommands.put("4", this::showAllComments);
    menuCommands.put("5", this::showDirectoryContent);
    menuCommands.put("5", this::showDirectoryContentNIO);
  }

  private void showSerializationExample() {
    logger.info("Please enter filepath for saving serialized object");
    String filePath = input.nextLine();
    try {
      controller.serializationExample(filePath);
      logger.info("---------------------------");
      controller.deserializationExample(filePath);
    } catch (IOException | ClassNotFoundException e) {
      logger.error("Error happen");
      logger.error(e.getMessage());
    }
  }

  private void showComparingUsualAndBufferedReaders() {
    logger.info("Please enter path to file for comparing");
    String filePath = input.nextLine();
    logger.info("Please enter buffer size");
    int bufferSize = input.nextInt();
    input.nextLine();
    try {
      controller.compareUsualAndBufferedReaders(filePath, bufferSize);
    } catch (IOException e) {
      logger.error("Error happen");
      logger.error(e.getMessage());
    }
  }

  private void showPushbackStream() {
    try {
      controller.pushBackStreamExample();
    } catch (IOException | IllegalArgumentException e) {
      logger.error("Something went wrong");
    }
  }

  private void showAllComments() {
    logger.info("Please enter path to source file with comments");
    String filePath = input.nextLine();
    try {
      List<String> comments = controller.getAllComments(filePath);
      comments.forEach(c -> logger.info(c));
    } catch (IOException e) {
      logger.error(e.getMessage());
    }
  }

  private void showDirectoryContent() {
    logger.info("Enter directory");
    String directory = input.nextLine();
    List<String> files = controller.showDirectoryContent(directory);
    files.forEach(f -> logger.info(f));
  }

  private void showDirectoryContentNIO() {
    logger.info("Enter directory");
    String directory = input.nextLine();
    try {
      List<String> files = controller.showDirectoryContentNIO(directory);
      files.forEach(f -> logger.info(f));
    } catch (IOException e) {
      logger.error(e.getMessage());
    }
  }

  public void show() {
    while (true) {
      try {
        menu.values().forEach(p -> logger.info(p));
        String commandKey = input.nextLine();
        menuCommands.get(commandKey).execute();
      } catch (InputMismatchException e) {
        logger.error("Bad input");
      }

    }
  }

  @Override
  public void eventOccurred(String message) {
    logger.info(message);
  }
}
