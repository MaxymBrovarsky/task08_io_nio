package com.brom.epam.nio.view;

public interface Command {
  void execute();
}
