package com.brom.epam.nio;

import com.brom.epam.nio.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
