package com.brom.epam.nio.controller;

public interface ControllerProgressListener {
  void eventOccurred(String message);
}
