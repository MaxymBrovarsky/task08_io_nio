package com.brom.epam.nio.controller;

import com.brom.epam.nio.model.Animal;
import com.brom.epam.nio.model.Dog;
import com.brom.epam.nio.utils.PushbackStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {
  private List<ControllerProgressListener> progressListeners;

  public Controller() {
    progressListeners = new ArrayList<>();
  }

  public void serializationExample(String fileName) throws IOException {
    progressListeners.forEach(l -> l.eventOccurred("Start Serialization"));
    progressListeners.forEach(l -> l.eventOccurred("Creating FileOutputStream"));
    FileOutputStream outputStream = new FileOutputStream(fileName);
    progressListeners.forEach(l -> l.eventOccurred("Creating ObjectOutputStream"));
    ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
    progressListeners.forEach(l -> l.eventOccurred("Creating Animal(dog)"));
    Animal animal = new Dog(30, 300,
        "Nature", "K2","Meat");
    progressListeners.forEach(l -> l.eventOccurred(animal.toString()));
    progressListeners.forEach(l -> l.eventOccurred("Saving animal object to file"));
    objectOutputStream.writeObject(animal);
    progressListeners.forEach(l -> l.eventOccurred("Finish Serialization"));
  }

  public void deserializationExample(String fileName) throws IOException, ClassNotFoundException {
    progressListeners.forEach(l -> l.eventOccurred("Start deserialization"));
    progressListeners.forEach(l -> l.eventOccurred("Creating FileInputStream"));
    FileInputStream inputStream = new FileInputStream(fileName);
    progressListeners.forEach(l -> l.eventOccurred("Creating ObjectInputStream"));
    ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
    progressListeners.forEach(l -> l.eventOccurred("Reading animal object from file"));
    Animal animal = (Animal) objectInputStream.readObject();
    progressListeners.forEach(l -> l.eventOccurred(animal.toString()));
    progressListeners.forEach(l -> l.eventOccurred("Finish deserialization"));
  }

  public void compareUsualAndBufferedReaders(final String filePath, int bufferSize) throws IOException {
    progressListeners.forEach(l -> l.eventOccurred("Comparing of usual and buffered readers"));
    readWithUsualReader(filePath);
    readWithBufferedReader(filePath, bufferSize);
  }

  private void readWithUsualReader(String filePath) throws IOException {
    progressListeners.forEach(l -> l.eventOccurred("Start reading with InputStreamReader"));
    long startTime = System.nanoTime();
    InputStreamReader inputStreamReader = new InputStreamReader(
        new FileInputStream(filePath)
    );
    int data;
    do {
      data = inputStreamReader.read();
    } while (data != -1);
    long finishTime = System.nanoTime();
    long elapsedTime = finishTime - startTime;
    progressListeners.forEach(l -> l.eventOccurred("Elapsed time = " + elapsedTime));
  }

  private void readWithBufferedReader(String filePath, int bufferSize) throws IOException {
    progressListeners.forEach(l -> l.eventOccurred("Start reading with BufferedStreamReader"));
    long startTime = System.nanoTime();
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
        new FileInputStream(filePath)
    ), bufferSize);

    String line;
    while ((line = bufferedReader.readLine()) != null);
    long finishTime = System.nanoTime();
    long elapsedTime = finishTime - startTime;
    progressListeners.forEach(l -> l.eventOccurred("Elapsed time = " + elapsedTime));
  }

  public void pushBackStreamExample() throws IOException, IllegalArgumentException {
    PushbackStream pushbackStream =
        new PushbackStream(new FileInputStream("test.txt"), 10);
    int data = pushbackStream.read();
    int finalData = data;
    progressListeners.forEach(l -> l.eventOccurred("read byte = " + finalData));
    progressListeners.forEach(l -> l.eventOccurred("unread 1 byte"));
    pushbackStream.unread(1);
    data = pushbackStream.read();
    int finalData1 = data;
    progressListeners.forEach(l -> l.eventOccurred("read byte = " + finalData1));
  }

  public List<String> getAllComments(String sourceFilePath) throws IOException {
    BufferedReader bufferedReader = new BufferedReader(new FileReader(sourceFilePath));
    StringBuilder sb = new StringBuilder();
    String line = null;
    while ((line = bufferedReader.readLine()) != null) {
      sb.append(line + "\n");
    }
    String text = sb.toString();
    boolean flag = false;
    ArrayList<Character> characters = new ArrayList<>();
    List<String> comments = new ArrayList<>();
    for (int i = 0; i < text.length(); i++) {
      if (flag) {
        if (text.charAt(i) == '/') {
          while (i < text.length() && text.charAt(i) != '\n') {
            characters.add(text.charAt(i));
            i++;
          }
          comments.add(characters.stream().map(String::valueOf).collect(Collectors.joining()));
          characters.clear();
        } else if(text.charAt(i) == '*') {
          while (i < text.length() && !(text.charAt(i) == '*' && text.charAt(i + 1) == '/')) {
            characters.add(text.charAt(i));
            i++;
          }
          comments.add(characters.stream().map(String::valueOf).collect(Collectors.joining()));
          characters.clear();
        }
        flag = false;
        continue;
      }
      if (text.charAt(i) == '/') {
        flag = true;
        continue;
      }
    }
    return comments;
  }

  public List<String> showDirectoryContent(String directory) {
    File dir = new File(directory);
    List<String> files = new ArrayList<>();
    getDirectoryContent(dir, files, "");
    return files;
  }

  private void getDirectoryContent(File directory, List<String> files, String tab) {
    for (File file: directory.listFiles()) {
      files.add(tab + file.getName());
      if (file.isDirectory()) {
        getDirectoryContent(file, files, tab+"\t");
      }
    }
  }

  public List<String> showDirectoryContentNIO(String directory) throws IOException {
    File dir = new File(directory);
    List<String> files;
//    getDirectoryContent(dir, files, "");
    try (Stream<Path> walk = Files.walk(Paths.get(directory))) {
      files = walk.filter(Files::isRegularFile).map(f -> f.toString()).collect(Collectors.toList());
    }
    return files;
  }

  public void addProgressListener(ControllerProgressListener listener) {
    this.progressListeners.add(listener);
  }
}
