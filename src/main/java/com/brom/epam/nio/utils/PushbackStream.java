package com.brom.epam.nio.utils;

import java.io.IOException;
import java.io.InputStream;

public class PushbackStream extends InputStream {
  private byte[] buffer;
  private int position;
  private int unreadCount;
  private InputStream in;

  public PushbackStream(InputStream in, int bufferSize) throws IllegalArgumentException {
    this.in = in;
    if (bufferSize <= 0) {
      throw new IllegalArgumentException("Wrong size of buffer");
    }
    this.buffer = new byte[bufferSize];
  }
  @Override
  public int read() throws IOException {
    if (unreadCount > 0) {
      return buffer[position - unreadCount--];
    }
    if (buffer.length == position) {
      position = 0;
      unreadCount = 0;
    }
    if (in == null) {
      return -1;
    }
    this.buffer[position++] = (byte) in.read();
    return buffer[position - 1];
  }

  public void unread(int size) throws IllegalArgumentException {
    if (size > position || size <= 0) {
      throw new IllegalArgumentException("size of buffer is smaller");
    }
    this.unreadCount+=size;
  }
}
