package com.brom.epam.nio.model;

import java.io.Serializable;

public class Animal implements Serializable {
  private int weightInKilograms;
  private int ageInDays;

  public Animal(int weightInKilograms, int ageInDays) {
    this.weightInKilograms = weightInKilograms;
    this.ageInDays = ageInDays;
  }

  @Override
  public String toString() {
    return "Animal{" +
        "weightInKilograms=" + weightInKilograms +
        ", ageInDays=" + ageInDays +
        '}';
  }
}
