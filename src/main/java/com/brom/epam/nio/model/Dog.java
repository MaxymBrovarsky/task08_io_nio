package com.brom.epam.nio.model;

public class Dog extends Animal {
  private String owner;
  private String nickname;
  private transient String favouriteFood;

  public Dog(int weightInKilograms, int ageInDays, String owner, String nickname,
      String favouriteFood) {
    super(weightInKilograms, ageInDays);
    this.owner = owner;
    this.nickname = nickname;
    this.favouriteFood = favouriteFood;
  }

  @Override
  public String toString() {
    return "Dog{" +
        "owner='" + owner + '\'' +
        ", nickname='" + nickname + '\'' +
        ", favouriteFood='" + favouriteFood + '\'' +
        '}';
  }
}
