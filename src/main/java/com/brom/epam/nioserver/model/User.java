package com.brom.epam.nioserver.model;

import java.nio.channels.SocketChannel;

public class User {
  private SocketChannel socketChannel;
  private String address;
  private String userName;


  public User(String address) {
    this.address = address;
  }

  public User(String address, String userName) {
    this.address = address;
    this.userName = userName;
  }

  public String getAddress() {
    return address;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public User(SocketChannel socketChannel, String address) {
    this.socketChannel = socketChannel;
    this.address = address;
  }

  public SocketChannel getSocketChannel() {
    return socketChannel;
  }

  @Override
  public String toString() {
    return "User{" +
        "address='" + address + '\'' +
        ", userName='" + userName + '\'' +
        '}';
  }
}
