package com.brom.epam.nioserver;

import com.brom.epam.nioserver.view.View;
import java.io.IOException;

public class Server {
  public static void main(String[] args) throws IOException {
    new View().start();
  }
}
