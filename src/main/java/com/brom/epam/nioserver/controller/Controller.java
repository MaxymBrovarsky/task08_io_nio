package com.brom.epam.nioserver.controller;

import com.brom.epam.nioserver.model.User;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class Controller {
  List<User> users = new ArrayList<>();

  public void processCommand(String command, String address) {
    String[] commandParts = command.split(";");
    switch (commandParts[0]) {
      case "SETNAME":
        if (commandParts.length == 2) {
          this.setUserName(address, commandParts[1]);
        }
        break;
      case "SENDMESSAGE":
        if (commandParts.length == 3) {
          this.sendMessage(commandParts[1], commandParts[2]);
        }
        break;
    }
  }

  public void createUser(SocketChannel address) throws IOException {
    users.add(new User(address, address.getRemoteAddress().toString()));
  }

  private void setUserName(String address, String name) {
    users.forEach(u -> {
      if (u.getAddress().equals(address)) {
        u.setUserName(name);
      }
    });
  }

  private void sendMessage(String name, String message) {
    users.forEach(u -> {
      if (u.getUserName().equals(name)) {
        try {
          u.getSocketChannel().write(ByteBuffer.wrap(message.getBytes()));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
  }
}
